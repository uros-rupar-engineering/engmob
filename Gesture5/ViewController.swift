//
//  ViewController.swift
//  Gesture5
//
//  Created by uros.rupar on 6/11/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        
        
        
        
        
        
        
        //tapGesture
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(self.tapAction(sender:)))
        
        tapgesture.numberOfTapsRequired = 1
        tapgesture.numberOfTouchesRequired = 1
        view.addGestureRecognizer(tapgesture)
        
        //pinch
        
        
        let pinchgesture = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchAction(sender:)))
        
        view.addGestureRecognizer(pinchgesture)
        //rotate
        //swipe
    }

    @IBOutlet weak var label: UILabel!
    
    let background =  UIColor(red: 55, green: 130, blue: 44, alpha: 1.0)
    
    
    
    //tapGesture
    //pinch
    //rotate
    //swipe
    
    
    @objc func tapAction(sender: UITapGestureRecognizer){
        if sender.state == .ended{
            label.text = "taped"
            
            view.backgroundColor = UIColor.red
        }
    }
    
    @objc func pinchAction(sender: UIPinchGestureRecognizer){
        if sender.state == .ended{
            label.text = "pinched"
            
            view.backgroundColor = UIColor.blue
        }
    }
    
    @objc func rotateAction(sender: UIRotationGestureRecognizer){
        if sender.state == .began{
            label.text = "rotate began"
            
            view.backgroundColor = UIColor.purple
        }
        
        if sender.state == .changed{
            label.text = "rotate changed"
            
            view.backgroundColor = UIColor.purple
        }
        
        
    }
    
    
}

